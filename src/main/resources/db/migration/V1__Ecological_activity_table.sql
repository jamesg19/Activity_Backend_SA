CREATE TABLE IF NOT EXISTS ecological_activity (
                                                   id BIGINT NOT NULL,
                                                   name VARCHAR(400) NOT NULL,
                                                   description VARCHAR(4000) NOT NULL,
                                                   state BOOLEAN NOT NULL,
                                                   publication_date DATE NOT NULL,
                                                   publication_expired DATE NOT NULL,
                                                   user_id BIGINT NOT NULL,
                                                   CONSTRAINT ecological_activity_pk PRIMARY KEY (id),
                                                   CONSTRAINT user_ecologial_activity_fk FOREIGN KEY (user_id) REFERENCES _user(user_id)
);

CREATE SEQUENCE SEQ_ECOLOGICAL_ACT START WITH 10000 INCREMENT BY 1;