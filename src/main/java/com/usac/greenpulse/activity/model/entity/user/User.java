package com.usac.greenpulse.activity.model.entity.user;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.usac.greenpulse.activity.model.entity.person.Person;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistence class for the _user database table.
 * @author willyrex
 */

@Entity
@Table(name = "_user")
@Getter
@Setter
@NoArgsConstructor
public class User {

    @Id
    @Column(name = "user_id")
    @SequenceGenerator(name = "userIdGen", sequenceName = "SEQ_USER", allocationSize = 1, initialValue = 10000)
    @GeneratedValue(generator = "userIdGen", strategy = GenerationType.SEQUENCE)
    private Long userId;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    @JsonManagedReference
    private Person person;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "role")
    private Integer role;

    @Column(name = "description")
    private String description;

    @Column(name = "profile_photo")
    private byte[] profilePhoto;

    @Column(name = "username")
    private String username;

    @Column(name = "accumulated_points")
    private int points;
}
