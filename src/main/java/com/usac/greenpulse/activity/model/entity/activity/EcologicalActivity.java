package com.usac.greenpulse.activity.model.entity.activity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.usac.greenpulse.activity.model.entity.user.UserAssignment;
import com.usac.greenpulse.activity.model.entity.user.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "ecological_activity")
public class EcologicalActivity {
    @Id
    @SequenceGenerator(name = "id", sequenceName = "seq_ecological_act", allocationSize = 1, initialValue = 10000)
    @GeneratedValue(generator = "id", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name", nullable = false, length = 400)
    private String name;

    @Column(name = "description", nullable = false, length = 4000)
    private String description;

    @Column(name = "state", nullable = false)
    private Boolean state;

    @Column(name = "publication_date", nullable = false)
    private LocalDate publicationDate;

    @Column(name = "publication_expired", nullable = false)
    private LocalDate publicationExpired;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonManagedReference
    private User user;

    @OneToMany(mappedBy = "activity")
    private Set<UserAssignment> userAssignments = new LinkedHashSet<>();

    @Column(name = "activity_photo")
    private byte[] activityPhoto;

}