package com.usac.greenpulse.activity.model.entity.user;

import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "user_assignment")
public class UserAssignment {
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "id", sequenceName = "seq_user_assignment", allocationSize = 1, initialValue = 10000)
    @GeneratedValue(generator = "id", strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "activity_id", nullable = false)
    private EcologicalActivity activity;

    @Column(name = "assignment_date", nullable = false)
    private LocalDate assignmentDate;

}