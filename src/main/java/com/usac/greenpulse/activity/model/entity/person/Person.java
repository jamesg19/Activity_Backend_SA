package com.usac.greenpulse.activity.model.entity.person;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
@Entity
@Table(name = "person")
public class Person {
    @Id
    @Column(name = "person_id")
    @SequenceGenerator(name = "personIdGen", sequenceName = "SEQ_PERSON", allocationSize = 1, initialValue = 10000)
    @GeneratedValue(generator = "personIdGen", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "first_name", nullable = false, length = 20)
    private String firstName;

    @Column(name = "last_name", nullable = false, length = 20)
    private String lastName;

    @Column(name = "birthday", nullable = false)
    private LocalDate birthday;

    @Column(name = "age", nullable = false)
    private Integer age;

}