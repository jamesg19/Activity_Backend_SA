package com.usac.greenpulse.activity.util;

import lombok.RequiredArgsConstructor;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.PublicKey;
import java.security.Security;

@Service
@RequiredArgsConstructor
public class KeyLoader {

    private final ResourceLoader resourceLoader;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public PublicKey loadPublicKey(String path) throws IOException {
        try (InputStream inputStream = this.resourceLoader.getResource(path).getInputStream()){
            try ( InputStreamReader reader = new InputStreamReader(inputStream)){
                PEMParser pemParser = new PEMParser(reader);
                JcaPEMKeyConverter keyConverter = new JcaPEMKeyConverter();
                return keyConverter.getPublicKey((SubjectPublicKeyInfo) pemParser.readObject());
            }
        }
    }

}
