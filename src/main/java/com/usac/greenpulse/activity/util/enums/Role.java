package com.usac.greenpulse.activity.util.enums;

public enum Role {

    ADMIN(1),
    GENERAL(2),
    ;

    Role(int value) {
        this.value = value;
    }

    public final int value;
}
