package com.usac.greenpulse.activity.util;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Base64;

public class Base64Converter {

    public static byte[] convertBase64(String base64) throws ParseException {
        String pattern = "data:{0};base64,{1}";
        MessageFormat messageFormat = new MessageFormat(pattern);
        Object[] decomposedDataUri = messageFormat.parse(base64);
        return Base64.getDecoder().decode(decomposedDataUri[1].toString());
    }

    public static String convertToString(byte[] bytes){
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static String convertToString(byte[] bytes, String format){
        return "data:"+format+";base64,"+Base64.getEncoder().encodeToString(bytes);
    }

}
