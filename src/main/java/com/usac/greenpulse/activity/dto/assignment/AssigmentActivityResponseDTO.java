package com.usac.greenpulse.activity.dto.assignment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usac.greenpulse.activity.dto.activity.ActivityUserAssigmentDTO;
import com.usac.greenpulse.activity.model.entity.user.UserAssignment;
import com.usac.greenpulse.activity.util.Format;
import lombok.Getter;

import java.time.LocalDate;

@Getter
public class AssigmentActivityResponseDTO {
    private final Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATE_FORMAT)
    private final LocalDate dateAssigment;
    private final ActivityUserAssigmentDTO activity;

    public AssigmentActivityResponseDTO(UserAssignment assignment){
        this.id = assignment.getId();
        this.dateAssigment = assignment.getAssignmentDate();
        this.activity = new ActivityUserAssigmentDTO(assignment.getActivity());
    }
}
