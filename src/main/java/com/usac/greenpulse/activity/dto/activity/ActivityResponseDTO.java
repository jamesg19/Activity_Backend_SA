package com.usac.greenpulse.activity.dto.activity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usac.greenpulse.activity.dto.user.UserActivityCreateDto;
import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import com.usac.greenpulse.activity.util.Format;
import com.usac.greenpulse.activity.util.Base64Converter;
import lombok.Getter;

import java.time.LocalDate;


@Getter
public class ActivityResponseDTO {
    private final Long id;
    private final String name;
    private final String description;
    private final Boolean state;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATE_FORMAT)
    private final LocalDate publicationDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATE_FORMAT)
    private final LocalDate publicationExpired;
    private final UserActivityCreateDto user;
    private String activityPhoto;

    public ActivityResponseDTO(EcologicalActivity activity){
        this.id = activity.getId();
        this.name = activity.getName();
        this.description = activity.getDescription();
        this.state = activity.getState();
        this.publicationDate = activity.getPublicationDate();
        this.publicationExpired = activity.getPublicationExpired();
        this.user = new UserActivityCreateDto(activity.getUser());
        if (activity.getActivityPhoto() != null) {
            this.activityPhoto = Base64Converter.convertToString(activity.getActivityPhoto(), "image/jpg");
        }
    }
}
