package com.usac.greenpulse.activity.dto.activity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usac.greenpulse.activity.util.Format;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
@Getter
@AllArgsConstructor
public class ActivityCreateRequestDTO {
    private String name;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATE_FORMAT)
    private LocalDate publicationExpired;
    private Long user;
    private String activityPhoto;
}
