package com.usac.greenpulse.activity.dto.activity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import com.usac.greenpulse.activity.util.Format;
import lombok.Getter;

import java.time.LocalDate;
@Getter
public class ActivityUserAssigmentDTO {
    private final Long id;
    private final String name;
    private final String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATE_FORMAT)
    private final LocalDate dateExpiration;
    private final String userCreate;
    private final byte [] activityPhoto;
    private final Boolean state;

    public ActivityUserAssigmentDTO(EcologicalActivity activity){
        this.id = activity.getId();
        this.name = activity.getName();
        this.description = activity.getDescription();
        this.dateExpiration = activity.getPublicationExpired();
        this.userCreate = activity.getUser().getPerson().getFirstName();
        this.activityPhoto = activity.getActivityPhoto();
        this.state = activity.getState();
    }
}
