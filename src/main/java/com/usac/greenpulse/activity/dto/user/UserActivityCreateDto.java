package com.usac.greenpulse.activity.dto.user;

import com.usac.greenpulse.activity.model.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserActivityCreateDto {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Long userId;

    public UserActivityCreateDto(User user){
        this.firstName = user.getPerson().getFirstName();
        this.lastName = user.getPerson().getLastName();
        this.email = user.getEmail();
        this.userId = user.getUserId();
    }
}
