package com.usac.greenpulse.activity.service.assigment;

import com.usac.greenpulse.activity.dto.assignment.AssigmentActivityResponseDTO;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.user.UserAssignment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AssigmentService {
    AssigmentActivityResponseDTO save(Long newAssgiment) throws ServiceException;
    List<AssigmentActivityResponseDTO> findAllByUser() throws ServiceException;
    void delete(Long activityId) throws ServiceException;
    UserAssignment findByIdActivityAndEmail(Long id, String email) throws ServiceException;
}
