package com.usac.greenpulse.activity.service.token;

import com.usac.greenpulse.activity.model.entity.token.TokenCredential;
import com.usac.greenpulse.activity.repository.token.TokenCredentialRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Setter
public class TokenCredentialServiceImpl implements TokenCredentialService{

    @Value("${token.valid.time.minutes}")
    private Long tokenValidTimeMinutes;

    private final TokenCredentialRepository tokenCredentialRepository;

    @Override
    public void create(String email, String token) {
        this.invalidateTokens(email, LocalDateTime.now());
        TokenCredential tokenCredential = new TokenCredential();
        tokenCredential.setUserEmail(email);
        tokenCredential.setToken(token);
        tokenCredential.setEntryDate(LocalDateTime.now());
        tokenCredential.setExpiryDate(LocalDateTime.now().plusMinutes(tokenValidTimeMinutes));
        this.tokenCredentialRepository.save(tokenCredential);
    }

    @Override
    public void incrementExpiryDate(TokenCredential tokenCredential) {
        tokenCredential.setExpiryDate(LocalDateTime.now().plusMinutes(tokenValidTimeMinutes));
        this.tokenCredentialRepository.save(tokenCredential);
    }

    @Override
    public void invalidateTokens(String email, LocalDateTime expiryDate) {
        List<TokenCredential> tokens = this.tokenCredentialRepository.findByUserEmailAndExpiryDateAfter(email, expiryDate);
        tokens.forEach(token -> {
            token.setExpiryDate(LocalDateTime.now().minusMinutes(1L));
            this.tokenCredentialRepository.save(token);
        });
    }

    @Override
    public Optional<TokenCredential> findByToken(String token) {
        return this.tokenCredentialRepository.findByToken(token);
    }

}
