package com.usac.greenpulse.activity.service.assigment;

import com.usac.greenpulse.activity.dto.assignment.AssigmentActivityResponseDTO;
import com.usac.greenpulse.activity.exceptions.DuplicatedEntityException;
import com.usac.greenpulse.activity.exceptions.NotFoundException;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import com.usac.greenpulse.activity.model.entity.user.User;
import com.usac.greenpulse.activity.model.entity.user.UserAssignment;
import com.usac.greenpulse.activity.repository.AssigmentRepository;
import com.usac.greenpulse.activity.service.activity.ActivityService;
import com.usac.greenpulse.activity.service.user.UserService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AssigmentServiceImpl implements AssigmentService{
    private final AssigmentRepository assigmentRepository;
    private final ActivityService activityService;
    private final UserService userService;

    @Autowired
    public AssigmentServiceImpl(AssigmentRepository assigmentRepository, ActivityService activityService, UserService userService){
        this.assigmentRepository = assigmentRepository;
        this.activityService = activityService;
        this.userService = userService;
    }
    @Transactional
    @Override
    public AssigmentActivityResponseDTO save(Long newAssgiment) throws ServiceException {
        User user = userService.userFindById();
        Optional<UserAssignment> assignmentFind = assigmentRepository.findByActivity_IdAndUser_Email(newAssgiment, user.getEmail());
        if(assignmentFind.isPresent()){
            throw new DuplicatedEntityException(String.format("This activity by id: %s already assigment you!",newAssgiment));
        }
        EcologicalActivity activity = activityService.findByIdNotDTO(newAssgiment);

        UserAssignment assignment = new UserAssignment();
        assignment.setUser(user);
        assignment.setActivity(activity);
        assignment.setAssignmentDate(LocalDate.now());

        user.setPoints(user.getPoints()+1);
        userService.setPoints(user);
        assignment = assigmentRepository.save(assignment);

        return new AssigmentActivityResponseDTO(assignment);
    }

    @Override
    public List<AssigmentActivityResponseDTO> findAllByUser() throws ServiceException {
        User user = userService.userFindById();
        return assigmentRepository.findAllByUser_Email(user.getEmail()).stream().map(AssigmentActivityResponseDTO::new).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void delete(Long activityId) throws ServiceException{
        User user = userService.userFindById();
        UserAssignment assigment = this.findByIdActivityAndEmail(activityId,user.getEmail());
        if(user.getPoints()>0){
            user.setPoints(user.getPoints()-1);
            userService.setPoints(user);
        }
        assigmentRepository.delete(assigment);
    }

    @Override
    public UserAssignment findByIdActivityAndEmail(Long id, String email) throws ServiceException {
        return assigmentRepository.findByActivity_IdAndUser_Email(id,email).orElseThrow(()->
                new NotFoundException(String.format("Activity with id: %s do not assigment!",id))
                );
    }
}
