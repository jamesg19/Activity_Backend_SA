package com.usac.greenpulse.activity.service.token;



import com.usac.greenpulse.activity.model.entity.token.TokenCredential;

import java.time.LocalDateTime;
import java.util.Optional;

public interface TokenCredentialService {

    void create(String email, String token);

    void incrementExpiryDate(TokenCredential tokenCredential);

    void invalidateTokens(String email, LocalDateTime expiryDate);

    Optional<TokenCredential> findByToken(String token);

}
