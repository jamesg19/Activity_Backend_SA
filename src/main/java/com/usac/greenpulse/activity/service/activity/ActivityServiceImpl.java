package com.usac.greenpulse.activity.service.activity;

import com.usac.greenpulse.activity.dto.activity.ActivityCreateRequestDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityResponseDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityUpdateRequestDto;
import com.usac.greenpulse.activity.exceptions.DuplicatedEntityException;
import com.usac.greenpulse.activity.exceptions.NotFoundException;
import com.usac.greenpulse.activity.exceptions.PaserExceptionActivity;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import com.usac.greenpulse.activity.model.entity.user.User;
import com.usac.greenpulse.activity.repository.ActivityRepository;
import com.usac.greenpulse.activity.service.user.UserService;
import com.usac.greenpulse.activity.util.Base64Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityServiceImpl implements ActivityService {
    private final ActivityRepository activityRepository;
    private final UserService userService;
    @Autowired
    public ActivityServiceImpl(ActivityRepository activityRepository, UserService userService){
        this.activityRepository = activityRepository;
        this.userService = userService;
    }
    @Override
    public ActivityResponseDTO save(ActivityCreateRequestDTO newActivity) throws ServiceException {
        if(existByName(newActivity.getName())){
            throw new DuplicatedEntityException(String.format("This ecological activity with name: %s, already exists!",newActivity.getName()));
        }
        User user = userService.userFindById();

        EcologicalActivity save = new EcologicalActivity();
        save.setName(newActivity.getName());
        save.setDescription(newActivity.getDescription());
        save.setState(true);
        save.setPublicationDate(LocalDate.now());
        save.setPublicationExpired(newActivity.getPublicationExpired());
        save.setUser(user);
        try {
            save.setActivityPhoto(Base64Converter.convertBase64(newActivity.getActivityPhoto()));
        } catch (Exception e){
            throw new PaserExceptionActivity("Picture not accepted");
        }
        save = this.activityRepository.save(save);
        return new ActivityResponseDTO(save);
    }

    @Override
    public ActivityResponseDTO findById(Long id) throws ServiceException {
        EcologicalActivity activity = this.findByIdNotDTO(id);
        return new ActivityResponseDTO(activity);
    }

    @Override
    public EcologicalActivity findByIdNotDTO(Long id) throws ServiceException {
        return activityRepository.findById(id).orElseThrow(()->new NotFoundException(String.format("This ecological activity with id:%s activity not exists!",id)));
    }

    @Override
    public ActivityResponseDTO update(Long id, ActivityUpdateRequestDto update) throws ServiceException {
        EcologicalActivity activity = this.findByIdNotDTO(id);
        if(this.exitsByNameNotId(activity.getName(), id)){
            throw new DuplicatedEntityException(String.format("This ecological activity with name: %s, already exists!",activity.getName()));
        }
        User user = userService.userFindById();
        activity.setName(update.getName());
        activity.setDescription(update.getDescription());
        activity.setState(update.getState());
        activity.setPublicationExpired(update.getPublicationExpired());
        activity.setUser(user);
        if(update.getActivityPhoto()!= null){
            try {
                activity.setActivityPhoto(Base64Converter.convertBase64(update.getActivityPhoto()));
            } catch (Exception e){
                throw new PaserExceptionActivity("Picture not accepted");
            }
        }
        activity = activityRepository.save(activity);
        return new ActivityResponseDTO(activity);
    }

    @Override
    public List<ActivityResponseDTO> findAll() {
        return activityRepository.findAll().stream().map(ActivityResponseDTO::new).collect(Collectors.toList());
    }

    @Override
    public Boolean existByName(String name) {
        return activityRepository.existsByName(name);
    }

    @Override
    public Boolean exitsByNameNotId(String name, Long id) {
        return activityRepository.existsByNameAndIdNot(name,id);
    }

    @Override
    public List<ActivityResponseDTO> findAllByState(Boolean state) {
        return activityRepository.findAllByState(state).stream().map(ActivityResponseDTO::new).collect(Collectors.toList());
    }

    @Override
    public List<ActivityResponseDTO> findAllNotAssigmentByEmail() throws ServiceException {
        User user = userService.userFindById();
        return activityRepository.findNotAssigmentById(user.getUserId(),LocalDate.now()).stream().map(ActivityResponseDTO::new).collect(Collectors.toList());
    }
    @Scheduled(cron = " 0 0 6 * * ?")
    public void setStateExpired(){
        List<EcologicalActivity> expiredAll = activityRepository.findAllByPublicationExpired(LocalDate.now());
        for(EcologicalActivity expired:expiredAll){
           expired.setState(false);
           activityRepository.save(expired);
        }
    }
}
