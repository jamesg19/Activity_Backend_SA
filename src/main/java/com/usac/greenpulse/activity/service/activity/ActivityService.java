package com.usac.greenpulse.activity.service.activity;

import com.usac.greenpulse.activity.dto.activity.ActivityCreateRequestDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityResponseDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityUpdateRequestDto;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;

import java.util.List;

public interface ActivityService {
    ActivityResponseDTO save(ActivityCreateRequestDTO newActivity) throws ServiceException;
    ActivityResponseDTO findById(Long id) throws ServiceException;
    EcologicalActivity findByIdNotDTO(Long id)throws  ServiceException;
    ActivityResponseDTO update(Long id, ActivityUpdateRequestDto update) throws ServiceException;
    List<ActivityResponseDTO> findAll();
    Boolean existByName(String name) throws ServiceException;
    Boolean exitsByNameNotId(String name, Long id) throws ServiceException;
    List<ActivityResponseDTO> findAllByState(Boolean state);
    List<ActivityResponseDTO> findAllNotAssigmentByEmail() throws ServiceException;
}
