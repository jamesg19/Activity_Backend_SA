package com.usac.greenpulse.activity.service.user;

import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.user.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    User userFindById() throws ServiceException;
    User setPoints(User userSetPoints);

}
