package com.usac.greenpulse.activity.service.person;

import com.usac.greenpulse.activity.exceptions.NotFoundException;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.user.User;
import com.usac.greenpulse.activity.repository.user.UserRepository;
import com.usac.greenpulse.activity.security.SecurityContext;
import com.usac.greenpulse.activity.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    @Autowired
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    @Override
    public User userFindById() throws ServiceException {
        SecurityContext auth = new SecurityContext();
        Long id = auth.getUserId();
        User user = userRepository.findById(id).orElseThrow(()->new NotFoundException(String.format("This user with id: %s not exist! ",id)));
        return user;
    }

    @Override
    public User setPoints(User userSetPoints) {
        return userRepository.save(userSetPoints);
    }

}
