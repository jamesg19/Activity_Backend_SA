package com.usac.greenpulse.activity.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${frontend.url}")
    private String frontendUrl;

    @Value("${frontend.full.url}")
    private String frontendFullUrl;


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/v1/**")
                .allowedOrigins(
                        "http://localhost:4200",
                        frontendUrl,
                        frontendFullUrl
                )
                .allowedMethods("GET", "PUT", "DELETE", "POST")
                .exposedHeaders("Total-Items")
        ;
    }

}
