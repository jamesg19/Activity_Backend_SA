package com.usac.greenpulse.activity.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtService {

    String extractUsername(String token);

    Boolean isTokenValid(String token, UserDetails userDetails);

}
