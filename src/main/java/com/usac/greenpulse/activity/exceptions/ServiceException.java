package com.usac.greenpulse.activity.exceptions;

public class ServiceException extends Exception{
    public ServiceException(String message){
        super(message);
    }
}
