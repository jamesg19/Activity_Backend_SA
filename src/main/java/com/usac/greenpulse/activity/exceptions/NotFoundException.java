package com.usac.greenpulse.activity.exceptions;

public class NotFoundException extends ServiceException {
    public NotFoundException(String message){
        super(message);
    }
}
