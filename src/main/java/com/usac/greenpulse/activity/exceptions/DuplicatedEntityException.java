package com.usac.greenpulse.activity.exceptions;

public class DuplicatedEntityException extends ServiceException{
    public DuplicatedEntityException(String message){
        super(message);
    }
}
