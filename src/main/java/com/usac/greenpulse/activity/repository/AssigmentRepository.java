package com.usac.greenpulse.activity.repository;

import com.usac.greenpulse.activity.model.entity.user.UserAssignment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssigmentRepository extends CrudRepository<UserAssignment,Long> {
    @Override
    List<UserAssignment> findAll();
    List<UserAssignment> findAllByUser_Email(String email);
    Optional<UserAssignment> findByActivity_IdAndUser_Email(Long id, String email);
}
