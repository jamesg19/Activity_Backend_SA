package com.usac.greenpulse.activity.repository.user;

import com.usac.greenpulse.activity.model.entity.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findFirstByEmail(String email);

}
