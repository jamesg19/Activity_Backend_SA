package com.usac.greenpulse.activity.repository;

import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
@Repository
public interface ActivityRepository extends CrudRepository<EcologicalActivity,Long> {
    List<EcologicalActivity> findAllByState(Boolean state);
    @Override
    List<EcologicalActivity> findAll();
    Boolean existsByName (String name);
    Boolean existsByNameAndIdNot(String name, Long id);
    @Query(value = "select e from EcologicalActivity e left join  UserAssignment ua on e.id = ua.activity.id and ua.user.userId = :id where ua.activity is null and e.publicationExpired >= :date")
    List<EcologicalActivity> findNotAssigmentById(@Param("id") Long id, @Param("date") LocalDate now);
    List<EcologicalActivity> findAllByPublicationExpired(LocalDate date);
}
