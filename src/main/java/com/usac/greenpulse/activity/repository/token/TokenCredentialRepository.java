package com.usac.greenpulse.activity.repository.token;

import com.usac.greenpulse.activity.model.entity.token.TokenCredential;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface TokenCredentialRepository extends CrudRepository<TokenCredential, Long> {

    Optional<TokenCredential> findByToken(String token);

    List<TokenCredential> findByUserEmailAndExpiryDateAfter(String email, LocalDateTime dateTime);

}
