package com.usac.greenpulse.activity.controller.exceptionHandler;

import com.usac.greenpulse.activity.exceptions.DuplicatedEntityException;
import com.usac.greenpulse.activity.exceptions.NotFoundException;
import com.usac.greenpulse.activity.exceptions.PaserExceptionActivity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handlerNotFountException(NotFoundException ex){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }
    @ExceptionHandler(DuplicatedEntityException.class)
    public ResponseEntity<String> handlerDuplicatedEntityException(DuplicatedEntityException ex){
        return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
    }
    @ExceptionHandler(PaserExceptionActivity.class)
    public ResponseEntity<String> handlerParseActivityException(PaserExceptionActivity ex){
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
    }
}
