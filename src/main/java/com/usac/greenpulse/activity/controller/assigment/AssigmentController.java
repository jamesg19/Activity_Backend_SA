package com.usac.greenpulse.activity.controller.assigment;

import com.usac.greenpulse.activity.dto.assignment.AssigmentActivityResponseDTO;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.service.assigment.AssigmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/v1/assigment")
@PreAuthorize("hasAnyAuthority('1','2')")
public class AssigmentController {
    private final AssigmentService assigmentService;
    public AssigmentController(AssigmentService assigmentService){
        this.assigmentService = assigmentService;
    }
    @PostMapping("/{id}")
    public ResponseEntity<AssigmentActivityResponseDTO> save(@PathVariable Long id) throws ServiceException {
        return ResponseEntity.ok(assigmentService.save(id));
    }

    @GetMapping
    public ResponseEntity<List<AssigmentActivityResponseDTO>> findAll() throws ServiceException{
        return ResponseEntity.ok(assigmentService.findAllByUser());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) throws ServiceException{
        assigmentService.delete(id);
        return ResponseEntity.accepted().build();
    }
}
