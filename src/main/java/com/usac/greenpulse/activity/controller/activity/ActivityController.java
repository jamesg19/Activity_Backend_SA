package com.usac.greenpulse.activity.controller.activity;

import com.usac.greenpulse.activity.dto.activity.ActivityCreateRequestDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityResponseDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityUpdateRequestDto;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.service.activity.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@PreAuthorize("hasAuthority('1')")
@RequestMapping("/v1/activity")
public class ActivityController {
    private final ActivityService activityService;
    @Autowired
    public ActivityController(ActivityService activityService){
        this.activityService = activityService;
    }
    @PostMapping
    public ResponseEntity<ActivityResponseDTO> save(@RequestBody ActivityCreateRequestDTO activity) throws ServiceException {
        return ResponseEntity.ok().body(activityService.save(activity));
    }
    @PutMapping("/{id}")
    public ResponseEntity<ActivityResponseDTO> update(@PathVariable Long id, @RequestBody ActivityUpdateRequestDto update) throws ServiceException{
        ActivityResponseDTO response = activityService.update(id, update);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/find-by-id/{id}")
    public ResponseEntity<ActivityResponseDTO> findById(@PathVariable Long id) throws ServiceException{
        return ResponseEntity.ok(activityService.findById(id));
    }
    @GetMapping("/findAll-by-state/{state}")
    public ResponseEntity<List<ActivityResponseDTO>> findAllByState(@PathVariable Boolean state){
        return ResponseEntity.ok(activityService.findAllByState(state));
    }
    @GetMapping("/findAll")
    public ResponseEntity<List<ActivityResponseDTO>> findAll(){
        return ResponseEntity.ok(activityService.findAll());
    }
    @PreAuthorize("hasAnyAuthority('1','2')")
    @GetMapping("/findAll-not-assigment")
    public ResponseEntity<List<ActivityResponseDTO>> findAllNotAssigment() throws ServiceException{
        return ResponseEntity.ok(activityService.findAllNotAssigmentByEmail());
    }
}
