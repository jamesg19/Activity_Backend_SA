package unit;

import com.usac.greenpulse.activity.dto.assignment.AssigmentActivityResponseDTO;
import com.usac.greenpulse.activity.exceptions.DuplicatedEntityException;
import com.usac.greenpulse.activity.exceptions.NotFoundException;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import com.usac.greenpulse.activity.model.entity.person.Person;
import com.usac.greenpulse.activity.model.entity.user.User;
import com.usac.greenpulse.activity.model.entity.user.UserAssignment;
import com.usac.greenpulse.activity.repository.AssigmentRepository;
import com.usac.greenpulse.activity.service.activity.ActivityService;
import com.usac.greenpulse.activity.service.assigment.AssigmentServiceImpl;
import com.usac.greenpulse.activity.service.user.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
public class AssigmentServiceImplTest {
    private AssigmentServiceImpl assigmentService;
    private final AssigmentRepository assigmentRepository = mock(AssigmentRepository.class);
    private final ActivityService activityService = mock(ActivityService.class);
    private final UserService userService = mock(UserService.class);
    private UserAssignment USER_ASSIGMENT;
    private final Long ID_ASSIGMENT = 1L;
    private final LocalDate ASSIGMENT_DATE = LocalDate.parse("2024-01-01");
    private User USER_ASSIGN_FOR_ACTIVITY;
    private final String FIRST_NAME = "FIRST_NAME";
    private final String EMAIL = "email@gamil.com";
    private final Integer POINTS = 1;
    private EcologicalActivity ACTIVITY;
    private final Long ID_ACTIVITY = 1L;
    private final String NAME_ACTIVITY = "NAME ACTIVITY 1";
    private final String DESCRIPTION = "DESCRIPTION ACTIVITY 1";
    private final LocalDate DATE_EXPIRATION = LocalDate.parse("2024-06-06");
    private final byte [] ACTIVITY_PHOTO = null;
    private final Boolean STATE = true;

    @BeforeEach
    void setUp(){
        assigmentService = new AssigmentServiceImpl(assigmentRepository,activityService,userService);

        Person PERSON_CREATE = new Person();
        PERSON_CREATE.setFirstName(FIRST_NAME);

        User USER_CREATE_ACTIVITY = new User();
        USER_CREATE_ACTIVITY.setPerson(PERSON_CREATE);

        ACTIVITY = new EcologicalActivity();
        ACTIVITY.setId(ID_ACTIVITY);
        ACTIVITY.setName(NAME_ACTIVITY);
        ACTIVITY.setDescription(DESCRIPTION);
        ACTIVITY.setPublicationExpired(DATE_EXPIRATION);
        ACTIVITY.setUser(USER_CREATE_ACTIVITY);
        ACTIVITY.setActivityPhoto(ACTIVITY_PHOTO);
        ACTIVITY.setState(STATE);

        USER_ASSIGN_FOR_ACTIVITY = new User();
        USER_ASSIGN_FOR_ACTIVITY.setUserId(1L);
        USER_ASSIGN_FOR_ACTIVITY.setEmail(EMAIL);
        USER_ASSIGN_FOR_ACTIVITY.setPoints(POINTS);

        USER_ASSIGMENT = new UserAssignment();
        USER_ASSIGMENT.setId(ID_ASSIGMENT);
        USER_ASSIGMENT.setActivity(ACTIVITY);
        USER_ASSIGMENT.setUser(USER_ASSIGN_FOR_ACTIVITY);
        USER_ASSIGMENT.setAssignmentDate(ASSIGMENT_DATE);
    }

    @Test
    void saveWithAssigmentDuplicated() throws ServiceException {
        when(userService.userFindById()).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        when(assigmentRepository.findByActivity_IdAndUser_Email(1L,EMAIL)).thenReturn(Optional.of(USER_ASSIGMENT));
        Assertions.assertThrows(DuplicatedEntityException.class,()-> assigmentService.save(1L));
    }
    @Test
    void saveSuccessful() throws ServiceException{
        when(userService.userFindById()).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        when(assigmentRepository.findByActivity_IdAndUser_Email(1L,EMAIL)).thenReturn(Optional.empty());
        when(activityService.findByIdNotDTO(ID_ACTIVITY)).thenReturn(ACTIVITY);

        USER_ASSIGN_FOR_ACTIVITY.setPoints(1);
        when(userService.setPoints(USER_ASSIGN_FOR_ACTIVITY)).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        when(assigmentRepository.save(any(UserAssignment.class))).thenReturn(USER_ASSIGMENT);

        AssigmentActivityResponseDTO expected = new AssigmentActivityResponseDTO(USER_ASSIGMENT);
        AssigmentActivityResponseDTO actually = assigmentService.save(ID_ACTIVITY);
        assertThat(actually).isEqualToComparingFieldByFieldRecursively(expected);
    }
    @Test
    void findAll() throws ServiceException{
        List<AssigmentActivityResponseDTO> expected = new ArrayList<>();
        expected.add(new AssigmentActivityResponseDTO(USER_ASSIGMENT));
        expected.add(new AssigmentActivityResponseDTO(USER_ASSIGMENT));
        expected.add(new AssigmentActivityResponseDTO(USER_ASSIGMENT));

        List<UserAssignment> list = new ArrayList<>();
        list.add(USER_ASSIGMENT);
        list.add(USER_ASSIGMENT);
        list.add(USER_ASSIGMENT);

        when(userService.userFindById()).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        when(assigmentRepository.findAllByUser_Email(EMAIL)).thenReturn(list);

        List<AssigmentActivityResponseDTO> actually = assigmentService.findAllByUser();

        assertThat(actually.size()).isEqualTo(expected.size());
        for (int i = 0; i < list.size(); i++) {
            assertThat(actually.get(i)).isEqualToComparingFieldByFieldRecursively(expected.get(i));
        }
    }

    @Test
    void deleteAssigmentNotFound() throws ServiceException{
        when(userService.userFindById()).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        when(assigmentRepository.findByActivity_IdAndUser_Email(ID_ACTIVITY,EMAIL)).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class,()->assigmentService.delete(ID_ASSIGMENT));
    }
    @Test
    void deleteAssigmentSuccessful() throws ServiceException{
        when(userService.userFindById()).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        when(assigmentRepository.findByActivity_IdAndUser_Email(ID_ACTIVITY,EMAIL)).thenReturn(Optional.of(USER_ASSIGMENT));
        when(userService.setPoints(any(User.class))).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        assigmentService.delete(ID_ASSIGMENT);
        verify(assigmentRepository,times(1)).delete(any(UserAssignment.class));

        when(userService.userFindById()).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        when(assigmentRepository.findByActivity_IdAndUser_Email(ID_ACTIVITY,EMAIL)).thenReturn(Optional.of(USER_ASSIGMENT));
        USER_ASSIGN_FOR_ACTIVITY.setPoints(0);
        when(userService.setPoints(any(User.class))).thenReturn(USER_ASSIGN_FOR_ACTIVITY);
        assigmentService.delete(ID_ASSIGMENT);
        verify(assigmentRepository,times(2)).delete(any(UserAssignment.class));
    }

}
