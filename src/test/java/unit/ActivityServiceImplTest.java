package unit;

import com.usac.greenpulse.activity.dto.activity.ActivityCreateRequestDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityResponseDTO;
import com.usac.greenpulse.activity.dto.activity.ActivityUpdateRequestDto;
import com.usac.greenpulse.activity.exceptions.DuplicatedEntityException;
import com.usac.greenpulse.activity.exceptions.NotFoundException;
import com.usac.greenpulse.activity.exceptions.ServiceException;
import com.usac.greenpulse.activity.model.entity.activity.EcologicalActivity;
import com.usac.greenpulse.activity.model.entity.person.Person;
import com.usac.greenpulse.activity.model.entity.user.User;
import com.usac.greenpulse.activity.repository.ActivityRepository;
import com.usac.greenpulse.activity.service.activity.ActivityServiceImpl;
import com.usac.greenpulse.activity.service.person.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import java.util.List;
public class ActivityServiceImplTest {
    private ActivityServiceImpl activityService;
    private final ActivityRepository activityRepository = Mockito.mock(ActivityRepository.class);
    private final UserServiceImpl userService = mock(UserServiceImpl.class);

    private EcologicalActivity ACTIVITY;

    private final Long ID = 1L;
    private final String NAME = "FIRST_ACTIVITY";
    private final String DESCRIPTION = "FIRST ACTIVITY FOR THIS NEW YEAR";
    private final Boolean STATE = true;
    private final LocalDate PUBLICATION_DATE = LocalDate.parse("2024-01-01");
    private final LocalDate PUBLICATION_EXPIRED = LocalDate.parse("2024-03-01");
    private User USER;
    private Long USER_ID = 1L;
    private String USER_EMAIL = "user@gmail.com";
    private Person PERSON;
    private Long PERSON_ID = 1L;
    private String FIRST_NAME = "FIRST_NAME";
    private String LAST_NAME = "LAST_NAME";
    private String ACTIVITY_PHOTO = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDw8NDw8PDQ0PDw8PDQ8NDw8NDw4NFREWFhURFhUYHSggGBomHRUVITEhJSkrLi4uFx8zODUsNygtLisBCgoKDg0OGhAQGislHx0tLS4tLy0tLS0vKy0vKystKy8rLS0tLSstLS0rLSstLS0tLS0rLS0rKy0tLSstLS0tK//AABEIAKgBLAMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAAAgEGAwQFB//EAEQQAAEDAgIGBwQGCAUFAAAAAAEAAgMEEQUSBiExQVFhEyIycYGRoUJScrEHI2LB0fAUM0NTY6LS4SSCkpOyFTRUc8L/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQMEAgUG/8QALREBAAIBAwMBBgYDAAAAAAAAAAECAwQRIRIxQVETMkJhkaEUInGB8PGxwdH/2gAMAwEAAhEDEQA/APUGhZGhQAsgChKQE4CgBMEDBMFAUhA4TBKEwQMEwUBSEDBMEoUqQ4TJAnRAQhCAQhCAQhCAQhCAQhCAQhCAQhCAQhCAQhCAQhCAQhCAQhCDhgJwoCYKEmCYKAmCCQmCgKQgYJglCYIGCYJEykMEyUKQUEpgUqkIg91K1KquhhF5ZY4h/Ee1vzXEqtOsPjuBK6UjdFG4jzNgqr5sdPetDm1617ysyFRpvpIh9immd8bmM+V1rO+kl26jHjMf6FTOuwR8X2lXOox+r0JC88H0kP8A/Eb/ALx/pWxF9I7fbpHj4ZQ75tCRrsE/F9pPxGP1XtCqdNp/RO7Ymi+KMOH8pK7NFj9HNYR1ETidjS7I7/S6xVtNRit2tDuMlJ7S6aEXQrnYQhCAQhCAQhCAQhCAQhCAQhCAQhCDihMFATBQkwTBKEwQMFIUBSEDBMlCkIGTJUXUh0XWvVVTImGSRwawbSd/Ic15zpBpfLUl0UBMVPsLh2n9x+/y2XWbUammGOVWXLXHHK341pfTU12A9PL+7iINu92wKlYnpjWz3DXCmYfZh1Otzeda4Ib66zvJPEpgF4mbXZMnnaPk8++ovb5NilwqapJcMrzfrOllLjfw1rox6Kz75IW/C0u9SFq0E7qd4fmyney2YuHAjcrhheMQziwIjf7rzrPduKYPZ34nuU6Z7q2dE5j+2i8YQVgm0JnOyVvg6oj+Rt6L0ADnYeQXAxbTKgpbgymeQX+rpgJTfgXXyg95WyMVIW9FYVN+glZ7MxHfLJJf+UW9VrSaI4ozsWl5Me3N/MApxb6Tql1xS08cDdz5rzSW421NB81SMU0qrqp2SWtqH32shcWN8WRgBdxgi3hMY9/CzVEVfAbSwvHHNGT6jUt7R5jqyR8WTK5kTpL7rgtAB77qjw4PC85m1bmSfxo3MN/iBJ9F659FGFSR01RJNL075JujY7OZAImMBsCde1x8lTfT0j+tnE4ocijx+opJOhbPJTyN/YzHqkcQHXaRzCuOE/SBsbVxZf4sNyO8sP3HwWxi2CQ1DejmibK3XbMNY5tI1tPcqlW6FSx3dRz9X9xVXczua8C48R4pWMmPnHb9pI66e7L1ihrop2CSGRsjDvYb2PAjceRWyvDKetqqKUXElHOdlyDHLbcHdl45L0fRfTKOqIhnAhqdg3Ryn7N9h5FbMGti09OSNp+zTj1EW4txK2IQhb2gIQhAIQhAIQhAIQhAIQhBxgpCUJgoScJgkCYIGCYJQpCBgpS3UZlIyXWKeoaxrnuOVrQXOJ3AbUr5LKoaa4o4NbTxnruLT/nLiI/Itc7vY1VZskY6TZxkv0V3VzTDSF1Q8tLujga4Rht9Zc42EfM8e7gLnmtbYAbgqRi+J9JUtEZvDTu+r+24HrSHmbeQCusEoexrxscAV89rK3ja1u8vLy7958ntcgDWTqA5rIZAzUzW72n8+DfxWEyZW5vafcN5M2E+OseBSEBgDpiWAi7Y226V47vZHM+AKz0xzbiHFazPZmgY57srGl7zuG4cTwHMqZqqng7Tv0mUfs4nWiaftSe13N81yMUxuzC0ltPB7jSeuftHbIfyAFzKOlrK02p4zDEf20w6xHFrfxXp4NB5s1Y9P6t7HdI5ZGWnmEUFrCFn1cduGUa3eN1xaVtTVG1JTOc397N1I/AbSr3gP0ewsIlmvUzb3y9bXyCvNHhLGAANAA4BepTDWrXXFEPL8M+jmSWzquZz/wCHH9XH3HeVcsO0QpoG5WRNaOQGvvVvZTgbkk7bBXbLNlFxvAoMpuweSsf0cta2iMbdjJ5B5hrvvXK0ik1FP9GlZc1MBOu4kb4Etd/8rJrI/LE/NTm7QvD2ArXfHrsd+w8f7raKV7QRYrz92dzaujZIwxyMbJG7tMeA5p8CqdjWiz4gZKbNJGNZiJLpYx9g7Xjket37Ffbeydu0HiPxWN7VzeIniXNqxLj6DaXmTLSVLrybIZT+0HuuPHnv+d8XluluBanVkAyyM68zW6swGsyttscNp47dt723QrHv0ymBefro+pLz1aneK26PPO/s7/tK/Bkn3LLKhKHJl6LSEIQgEIQgEIQgEIQg4oTBKFIUJOEwSBMgZTdLdQSgYlYnyIc5alRLZSMdXU2C800yryBVz31xsyR8nSBsY9OkPirbi1XYFefaRnpKaoG8ywE/CCfvKxa33a+m8M2p7R+qj4fF7RVh0UrSWOpnXzCxj3mxNtX53LmSxZGADvW/g8ZZUBzTkc5kjI372dI3qvHMXBWXN03pMT/Nme/McrJWTiJxDbOlbZoOpzIQNVmjYXcTsBJtxXCE0tQ8x0zTNISc8riTG07yT7R7vNdDAdFqmdrW1DyIRtaCc8nJzju3WXoNBhlPSxj9XCwDa4hg8ytOHT0pXeWjHjrWN5VXAdBWhwmqSaibi/st5NbsCvlFhrWAAAC3BaEmk9DFq6QyEbomOd66h6rCdOqcdmGY/Fkb95Vs6rDX4nc5sceVoigAWw1iqDNPod9PIO57Styn05o3anCaPm5gcP5SSojWYZ+IjUY/VZHBaVVsWanropRdjwb7AQWO/wBLrFY6kalfW0W7Sti0T2UzSEbVXNFMS/Rq5rnGzC7K/wCB+onwNj4K1Y/HqK89xTqSh+46iuM9Oukw4yRvV71dQSq/objAqqVtzeWIBknEi3Vd4geYK7uZeMx7olGrVtGsd6W4IBGwi/8AZSXLFGdRHBx8iL/O6nwByqWAf4HFn0zdUMzc0Y3BhGZo8Ou3wVscVVdIGWxOgeO1kynm0Pd/U5RE9Mxb0lE8TE+j0hrlkDlpwvuAthpXvPQZwVKxgpgUDIUAqVIEIQgEIQg4gTJAmChJgpBUBF0DXSkoSuKDHI5c6sk1FbspXLrTqKCsY5NtVUDruex3ZlGQk7A692HzHqrHje9Vp0YcS07HAjuO4+azaqu9NlGaN6ubX0ZPVAN+AFymp6dwDQWuJbsdbLqvcDetmV7pGljgDNFqkBHbb7w/PPjbDQmKQ5HNyu2NIJab8Db5/k+Va09P6MNrS7Qx2qyhjHCMAWu1ozHvJ2ei0pM8hzSPdI7i5xefMpjQkdmSRv8AmLh6qDFMNjmPHB7cvq1UWydXn6q5tM+Utj8PUpxH+TqSCpc3txuaOMdnjy2rNDPG7suBPC+V3kVVPUhGRbFBDmljG7OPmjLbd5rNRutJGeD2/Nc1n80FZ5W4xrLFVSM1Zi5vuv6w/t4J8qgsXrxbZtYK0CZpA6rvdJ1HuP4qgY7SnrMILXDcRYgr0MxLSxHDWTNyyC5As1w1Pb3HeORWimomOJWVyT5UHRLSB1HOHG5b2JWe8y+vx3hey01WyVjZY3B7Hi7SN4XkGL6GVTX54ckjSQOseicfA6vIrJohpUaKV1PI9s1PmsXRnO1rt7mHeN3A21bis2akb9VVN9t94evFyRju33x/Jyw09QyRrZGOD2OF2uabghZIhfNzcB5D+6phEGvdVPphV4hDMw5oo2zFjhscxtmNcORe4kcitbSfSLpnGipTdhOSeZp7e4xMPDi7vAXUwKmEZaPaLGeETb5B3klzvBq6pTrvEER1WiFzp3aluNK51MVvMK9t6DOCmBWMFMED3U3SXUqA90XSKUQa6i6hCDjJkoUhEmUpUIGJSOUpXIMEq51WF0pFpVDUFTxeK4Kqsws7uKveIQXuqliVMQSbLi9d4cWjdzq2lL8skfVlb2CPaHud/D835b2iU5mDJOO1HsDvh/DyXYgkt1XbD6c0lfh4kN7hsvsv1Bsnfwd6Hlv8vJWaSxXrsMNxAPGR+p41a9/I810C1VyXM12Wdrg8aukA645OB7XjrXQpayRo/fsG9mt7R3bfPzKwZcHmn0/4zWrs6RZy+5YpaRju0GnvFyiCtik7Lhfg7qkeBWxZZt7Vnnhy0hSFv6uSRnK92+RumaJxrvG/4mlvyK3Ldyi3ip9pPkdah0hlsGyxw34gyjVztf5Lt0uIRv3w34CQj0IBVOJ8B5LXkrIm9p455esVfTUW7bb/AFW1yS9Ec6wzERtHEk29SsNZXxQR9NNK2GPcSAwu5NAFyV5xJpQIv+2hHSbppQHuHNrdjT5qt1dZPVyXc59RKdVySWt5X3dw9Fux9Vo5jZdEzKwaXaUPqWuZBeOB92kuP107N7fstPAbd5OxU8RuYcwBtvadR8Oas9Foo5wDpCXPtqGwMH53eaSsw6SHUW54+YvZaYrtHCzp4bmiWkclMRlPSQk/WRk+o4OVw0rxt7qVkVIbmpGZzwbOEJ1EDgTYg8ADxVBw+jbI8CJjhKQHWAdZzN5PLbr2KwukghyMZ/iZWgtaNrA47bkdr4Rq567LHaJ6tqqZjxDPgdE2nYyWQZnEZYIt8h3m25t1c8GhcLuecz3nM88+HduVeweje9/SynPKdRJ2MHujd5bNiuVFFYBbtLj+Jpw18ujAFuMWtCFstW5pZQnBWMJwpDKUqEDIuoQgm6LqLqLqByVN0qEDqUgKlAyUoQUGNwWtK1bZCxPag5NTDdcHEKO99Stcsa0Kinug89raMtNwsMNRbqvF28CrfWUN76lwK3DeAVOTFFldqbtd8Qe2xHTR21Am0jB9k8ORuFznYSSc1O+7hryHqSjw3+F1mLXxnVdZf0lj9UjdfvDUV5+TTzHZltjlyJy656aPMRqLtbH+Lht8VMb2+xNJHyeMw82/gu9ncRYOZO3c2YXcO520ea1JqaA9uOWA8W2mZ9zvUqiaz5hTNGkHz+zNn+Es+RN0kklQNvTeWUfJZ3YXG79XURHk8uid5OH3oGDVI1sBcNxY9rv+JKr2rHf/AA56XHnrR7UgvwuXny1rXM7namMc88XdUfirAaWtG6o8pT9ygRVvuz+Tx9yti9Y/t1G0OZR4JNMeueqfZb1W+O8q64LgcUDQXlkQ+0Q0+uxcAUVe/dNb7TnAJH4RIP1ssMQ4ySNB8ibqfbfN11LpLjFDELdK11t0YLz6Lh4hpLC7VHBn4Gawbf4Rt81yY6KmG2WScj2YI3ZT/mdYeRXQpITshhZF9uT66S3/ABHquote3aP9Ot7SwH9JnBdI4QQk67jo2OPDKNbz33K7OEYYB2GuHGR+qR3cPYHr3Law/CCXB7y57/eecxtwHAcgrPRUQbuWjHppnmy6mKZ7ow6jDQBZdqCNJBFZbcbV6FY2jZpiNmRgWZqRoThSk4TBKpQMhRdCBlCLoQShQoug5IUhKpCBlKVSglChCASEJ1BQYXNWCSNbZCRzUHMmp1z6iivuXfcxYXwoKjVYYDuXIqcI4BXySmutaSiB3KJiJRMPPZMPe3ZdKHzN4kc1epMOB3LVfhQ4Ku2GsuJxxKmumv2ogfCyxFsR19EQeRVwdg44Jf8Aoo4KqdNDj2SpAgbDM34ZHj5FSMx2OqP96T8Vbm4KOCzx4MOCj8LB7GFMFGX7Wud/7Huf81t0uDHcxrfhaFc4sKA3Lchw8Dcu409YdRihWKTBeIuu5SYYBuXXipANy244FbXHWHcViGnT0lty34oVlZGszWrt0hjFlaEAJgEDBMFAUoGCFCEDIUIuglF1CEE3QoRdByVKVSgYKbpQpQSpSqboJQhCCCFBCZCDGWpS1ZbIsg1zGkMS2sqMqDTMKU063ciMiDQ/Rgp/Rgt7IpyINEUwWRtOtvImDEGsIVkbEs4apDUGNrFkDU4amAQQGpgFICkBAAJlClBKlQhBKEIQShQhBKFCEEoUIQckKUIQSEyEIBCEIJClCFIlQpQgApsoQgmyLIQgLIshCCbKbIQgmykNQhBICkBShBIClCEEqUIQClShQBCEKQIQhAIQhAIQhAIQhB//2Q==";

    @BeforeEach
    void setup (){
        this.activityService = new ActivityServiceImpl(activityRepository,userService);

        this.PERSON = new Person();
        this.PERSON.setId(PERSON_ID);
        this.PERSON.setFirstName(FIRST_NAME);
        this.PERSON.setLastName(LAST_NAME);

        this.USER = new User();
        this.USER.setUserId(USER_ID);
        this.USER.setEmail(USER_EMAIL);
        this.USER.setPerson(PERSON);

        this.ACTIVITY = new EcologicalActivity();
        this.ACTIVITY.setId(ID);
        this.ACTIVITY.setName(NAME);
        this.ACTIVITY.setDescription(DESCRIPTION);
        this.ACTIVITY.setState(STATE);
        this.ACTIVITY.setPublicationDate(PUBLICATION_DATE);
        this.ACTIVITY.setPublicationExpired(PUBLICATION_EXPIRED);
        this.ACTIVITY.setUser(USER);
    }

    @Test
    public void throwSaveWithNameExists() throws ServiceException{
        when(activityRepository.existsByName(NAME)).thenReturn(true);
        ActivityCreateRequestDTO request = new ActivityCreateRequestDTO(NAME,DESCRIPTION,PUBLICATION_EXPIRED,USER_ID,null);
        Assertions.assertThrows(DuplicatedEntityException.class,()-> activityService.save(request));
    }
    @Test
    public void saveSuccessful()throws ServiceException{
        when(activityRepository.existsByName(NAME)).thenReturn(false);
        when(userService.userFindById()).thenReturn(USER);
        ActivityCreateRequestDTO request = new ActivityCreateRequestDTO(NAME,DESCRIPTION,PUBLICATION_EXPIRED,USER_ID,ACTIVITY_PHOTO);
        when(activityRepository.save(any(EcologicalActivity.class))).thenReturn(ACTIVITY);

        ActivityResponseDTO expected = new ActivityResponseDTO(ACTIVITY);
        ActivityResponseDTO actually = activityService.save(request);

        assertThat(actually).isEqualToComparingFieldByFieldRecursively(expected);
    }
    @Test
    public void findByIdIfNotExist(){
        when(activityRepository.findById(ID)).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class,()-> activityService.findById(ID));
    }
    @Test
    public void findById() throws ServiceException{
        when(activityRepository.findById(ID)).thenReturn(Optional.of(ACTIVITY));

        ActivityResponseDTO expected = new ActivityResponseDTO(ACTIVITY);
        ActivityResponseDTO actually = activityService.findById(ID);
        assertThat(actually).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @Test
    public void updateWithNameRepeated(){
        when(activityRepository.findById(ID)).thenReturn(Optional.of(ACTIVITY));
        when(activityRepository.existsByNameAndIdNot(NAME,ID)).thenReturn(true);

        Assertions.assertThrows(DuplicatedEntityException.class,()-> activityService.update(ID,any(ActivityUpdateRequestDto.class)));
    }
    @Test
    public void updateSuccessful() throws ServiceException{
        when(activityRepository.findById(ID)).thenReturn(Optional.of(ACTIVITY));
        when(activityRepository.existsByNameAndIdNot(NAME,ID)).thenReturn(false);

        String UPDATE_NAME = "UPDATE";
        String UPDATE_DESCRIPTION = "UPDATE DESCRIPTION";
        LocalDate UPDATE_EXPIRED = LocalDate.parse("2025-01-01");
        Boolean UPDATE_STATE = false;

        EcologicalActivity updateActivity = new EcologicalActivity();
        updateActivity.setId(ID);
        updateActivity.setName(UPDATE_NAME);
        updateActivity.setDescription(UPDATE_DESCRIPTION);
        updateActivity.setPublicationDate(PUBLICATION_DATE);
        updateActivity.setPublicationExpired(UPDATE_EXPIRED);
        updateActivity.setState(UPDATE_STATE);
        updateActivity.setUser(USER);
        when(activityRepository.save(ACTIVITY)).thenReturn(updateActivity);

        ActivityResponseDTO expected = new ActivityResponseDTO(updateActivity);

        ActivityUpdateRequestDto request = new ActivityUpdateRequestDto(UPDATE_NAME,UPDATE_DESCRIPTION,UPDATE_EXPIRED,UPDATE_STATE,null);
        ActivityResponseDTO actually = activityService.update(ID,request);

        assertThat(actually).isEqualToComparingFieldByFieldRecursively(expected);
    }
    @Test
    void findAllTest(){
        List<EcologicalActivity> list = new ArrayList<>();
        list.add(ACTIVITY);
        list.add(ACTIVITY);
        list.add(ACTIVITY);
        when(activityRepository.findAll()).thenReturn(list);

        List<ActivityResponseDTO> expected = new ArrayList<>();
        expected.add(new ActivityResponseDTO(ACTIVITY));
        expected.add(new ActivityResponseDTO(ACTIVITY));
        expected.add(new ActivityResponseDTO(ACTIVITY));

        List<ActivityResponseDTO> actually = activityService.findAll();
        assertThat(actually.size()).isEqualTo(expected.size());
        for (int i = 0; i < expected.size(); i++) {
            assertThat(actually.get(i)).isEqualToComparingFieldByFieldRecursively(expected.get(i));
        }
    }

    @Test
    void findAllActive(){
        ACTIVITY.setState(true);
        List<EcologicalActivity> list = new ArrayList<>();
        list.add(ACTIVITY);
        list.add(ACTIVITY);
        list.add(ACTIVITY);
        when(activityRepository.findAllByState(true)).thenReturn(list);

        List<ActivityResponseDTO> expected = new ArrayList<>();
        expected.add(new ActivityResponseDTO(ACTIVITY));
        expected.add(new ActivityResponseDTO(ACTIVITY));
        expected.add(new ActivityResponseDTO(ACTIVITY));

        List<ActivityResponseDTO> actually = activityService.findAllByState(true);

        assertThat(actually.size()).isEqualTo(expected.size());
        for (int i = 0; i < actually.size(); i++) {
            assertThat(actually.get(i)).isEqualToComparingFieldByFieldRecursively(expected.get(i));
        }
    }
}
