FROM alpine/java:21.0.1

WORKDIR /app

COPY target/*.jar /app/green-pulse-activity.jar

ENTRYPOINT ["java", "-jar", "/app/green-pulse-activity.jar"]