INSERT INTO ecological_activity (id,name, description, state, publication_date, publication_expired,user_id)
VALUES
    (100,'Primera siembra anual','Primera siembra del año para nuevas costumbres hacia la familia y la naturaleza',true,'2024-05-03','2024-07-01',100),
    (101,'5k por reforestacion','Primera carrera en beneficio a reforestacion el cerro la cruz Antigua Guatemala',true,'2024-05-03','2024-05-09',100)
;