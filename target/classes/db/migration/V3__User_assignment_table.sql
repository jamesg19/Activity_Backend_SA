CREATE TABLE IF NOT EXISTS user_assignment(
                                              id BIGINT,
                                              user_id BIGINT NOT NULL,
                                              activity_id BIGINT NOT NULL,
                                              assignment_date DATE NOT NULL,
                                              CONSTRAINT user_assignment_pk PRIMARY KEY (id),
                                              CONSTRAINT user_assigment FOREIGN KEY (user_id) REFERENCES _user(user_id),
                                              CONSTRAINT activity_assigment FOREIGN KEY (activity_id) REFERENCES ecological_activity(id)
);

CREATE SEQUENCE SEQ_USER_ASSIGNMENT START WITH 10000 INCREMENT BY 1;